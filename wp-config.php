<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'artesanale' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'D6/NPI{r;S<8{=XG$MXoZd+Wbk=Pk,d_*g{BF?/,Nj1tr]%&uTYn}h4w`Dlby3I8' );
define( 'SECURE_AUTH_KEY',  'NNcB-66h[5jOZw`,X.M[r.^?EWC@l?Xhd{=cC@|]Ijpl*0WA,o?xqrw(3_)$ZgO6' );
define( 'LOGGED_IN_KEY',    '>8F6cQ8^&=`S+YpH1Fm8+f>??_CLT>w8V[ ur|6MKzERl7R@ZrN (S][hjJHWx<K' );
define( 'NONCE_KEY',        '<o{`kI/ji;EGR95Ns_B|W1-nTha*r giK c$`,Z+-Y(nJmRHM`w5amKSb9S&9,d]' );
define( 'AUTH_SALT',        '0@bhOi)vu|>31`~2=e=Gmy@yRx%:}l2 HYUPD:;^Ldfs2tIe;Cu%Da/HWqd+`pV5' );
define( 'SECURE_AUTH_SALT', '^RR3MfDg76f+6x%jJ!Pd4b!WqS&kXFh8@2ePEu{<e^1AnVrO^*uOIF90JW[Qo/eM' );
define( 'LOGGED_IN_SALT',   'yW?L^r$M3ZQ%JP,`:eE!l(>:$gc1!?Dwx!mUlZpXJ5~Iif[H9H~/BE&%&2?6swsv' );
define( 'NONCE_SALT',       'C%I$OHTZis_uPTPa2|T>&<t}if1_jrco|OAJVxsN/v.=(3-1qG5ptJc>kB5VP$y.' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
